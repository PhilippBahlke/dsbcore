/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.core.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author Selvyn
 */
public class DBConnector
{
    static  private DBConnector itsSelf = null;
    
    private Connection itsConnection;
    private String  dbDriver ="";   //  "com.mysql.jdbc.Driver";
    private String  dbPath = "";    //  "jdbc:mysql://52.209.91.145/";
    private String  dbName = "";    //  "db_grad_cs_1916";
    private String  dbUser = "";    //  "selvyn";
    private String  dbPwd = "";     //  "dbGradProg2016";

    static  public  DBConnector getConnector() throws IOException
    {
        if( itsSelf == null )
             itsSelf = new DBConnector();
        return itsSelf;
    }
    private DBConnector(){}
    
    public  Connection  getConnection()
    {
        return itsConnection;
    }
    
    public  boolean    connect( Properties properties )
    {
        boolean result = false;
        try
        {
            MainUnit.log("On Entry -> DBConnector.connect()");
            
            dbDriver = properties.getProperty("dbDriver");
            dbPath = properties.getProperty("dbPath");
            dbName = properties.getProperty("dbName");
            dbUser = properties.getProperty("dbUser");
            dbPwd = properties.getProperty("dbPwd");
          
            Class.forName( dbDriver );

            //dbPath =  "jdbc:mysql://172.16.8.189:3306/";
            
            itsConnection = DriverManager.getConnection(dbPath + dbName, 
                                                    dbUser, 
                                                    dbPwd );

            MainUnit.log( "Successfully connected to " + dbName );
            
            result = true;
        }
        catch( ClassNotFoundException | SQLException e )
        {
           e.printStackTrace();
        }
        
        MainUnit.log("On Exit -> DBConnector.connect()");

        return result;
    }
}
