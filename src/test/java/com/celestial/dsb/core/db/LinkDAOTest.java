/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.core.db;

import com.celestial.dsb.core.ApplicationScopeHelper;
import domain.Bookmark;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Selvyn
 */
public class LinkDAOTest
{
    
    public LinkDAOTest()
    {
    }
    
    @Before
    public void setUp()
    {
        ApplicationScopeHelper ash = new ApplicationScopeHelper();
        ash.bootstrapDBConnection();
    }

    /**
     * Test of saveLink method, of class LinkDAO.
     */
    @Test
    //@Ignore
    public void testSaveLink() throws Exception
    {
        System.out.println("saveLink");
        String url = "www.tinyurl.com";
        String description = "shorten links";
        ArrayList<String> tags = new ArrayList<String>(){{add("links"); add("compressed");}};
        LinkDAO instance = new LinkDAO();
        instance.saveLink(url, description, tags);
    }

    /**
     * Test of getAllLinks method, of class LinkDAO.
     */
    @Test
    //@Ignore
    public void testGetAllLinks()
    {
        System.out.println("getAllLinks");
        LinkDAO instance = new LinkDAO();
        ArrayList<Bookmark> expResult = null;
        ArrayList<Bookmark> result = instance.getAllLinks();
        
        for( Bookmark bm: result )
        {
            System.out.println( bm.getUrl() + ", " + bm.getDescription());
        }
    }

    /**
     * Test of getLinksForTag method, of class LinkDAO.
     */
    @Test
    //@Ignore
    public void testGetLinksForTag()
    {
        System.out.println("getLinksForTag");
        String metatag = "java";
        LinkDAO instance = new LinkDAO();
        //ArrayList<Bookmark> expResult = null;
        ArrayList<Bookmark> result = instance.getLinksForTag(metatag);
        
        for( Bookmark bm: result )
        {
            System.out.println( bm.getUrl() + ", " + bm.getDescription());
        }
    }
    
}
