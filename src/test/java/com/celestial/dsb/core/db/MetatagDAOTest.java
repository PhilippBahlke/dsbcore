/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.core.db;

import com.celestial.dsb.core.ApplicationScopeHelper;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;

/**
 *
 * @author Selvyn
 */
public class MetatagDAOTest
{
    
    public MetatagDAOTest()
    {
    }
    
    @Before
    public void setUp()
    {
        ApplicationScopeHelper ash = new ApplicationScopeHelper();
        ash.bootstrapDBConnection();
    }

    /**
     * Test of saveTag method, of class MetatagDAO.
     */
    @Test
    //@Ignore
    public void testSaveTag() throws Exception
    {
        System.out.println("saveTag");
        String tagname = "java";
        MetatagDAO instance = new MetatagDAO();
        instance.saveTag(tagname);
    }

    /**
     * Test of getAllTags method, of class MetatagDAO.
     */
    @Test
    //@Ignore
    public void testGetAllTags()
    {
        System.out.println("getAllTags");
        MetatagDAO instance = new MetatagDAO();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getAllTags();
        
        for( String tag: result )
        {
            System.out.println( tag );
        }
    }

    /**
     * Test of getTagsForLink method, of class MetatagDAO.
     */
    @Test
    //@Ignore
    public void testGetTagsForLink()
    {
        System.out.println("getTagsForLink");
        int linkId = 2;
        MetatagDAO instance = new MetatagDAO();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getTagsForLink(linkId);
        
        for( String tag: result )
        {
            System.out.println( tag );
        }
    }
    
}
